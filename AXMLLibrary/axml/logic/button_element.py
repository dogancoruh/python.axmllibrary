from axml.data.element import Element

class ButtonElement(Element):
    text = ""
    image = None

    def __init__(self):
        self.text = ""
        self.image = None