import xml.etree.ElementTree

class AXMLParser:
    def __init__(self):
        return

    @staticmethod
    def parsefile(filename):
        rootElement = xml.etree.ElementTree.parse(filename).getroot()

        layout = AXMLLayout()

        return layout
